import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.zemaprod"
version = "0.1.1"

buildscript {

    extra.set("kotlinVersion", "1.4.0")
    extra.set("springBootVersion", "2.4.5")
    extra.set("sourceCompatibility", "15")

    repositories {
        mavenCentral()
        maven("https://repo.spring.io/milestone")
    }

    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${project.extra["springBootVersion"]}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.extra["kotlinVersion"]}")
        classpath("org.jetbrains.kotlin:kotlin-allopen:${project.extra["kotlinVersion"]}")
        classpath("org.jetbrains.kotlin:kotlin-noarg:${project.extra["kotlinVersion"]}")
    }
}

repositories {
    mavenCentral()
    maven("https://repo.spring.io/snapshot")
    maven("https://repo.spring.io/milestone")
}


plugins {
    kotlin("jvm")
    kotlin("plugin.spring")
    kotlin("plugin.jpa")
    eclipse
    idea
    id("org.springframework.boot") version "2.4.1"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
}


dependencies {
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
    implementation("org.springframework.cloud:spring-cloud-starter-gateway")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-validation")

}

configure<io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension> {
    imports(delegateClosureOf<io.spring.gradle.dependencymanagement.dsl.ImportsHandler> {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:2020.0.2")
    })
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "${project.extra["sourceCompatibility"]}"
        kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
    }
    getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
        classpath(configurations["developmentOnly"])
    }
}


