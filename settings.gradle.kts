rootProject.name = "gateway"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("org.jetbrains.kotlin.")) {
                useVersion("1.3.61")
            }
        }
    }
}
