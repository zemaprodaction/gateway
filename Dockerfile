From gradle:6.7-jdk11-openj9 AS grad
WORKDIR gateway
COPY  . .
RUN gradle build

FROM openjdk:15-slim AS builder
WORKDIR gateway
COPY --from=grad home/gradle/gateway/build/libs/*.jar statistic.jar
RUN java -Djarmode=layertools -jar statistic.jar extract


FROM openjdk:15-slim
WORKDIR gateway
RUN java -version
COPY --from=builder gateway/dependencies/ ./
COPY --from=builder gateway/spring-boot-loader/ ./
COPY --from=builder gateway/snapshot-dependencies/ ./
COPY --from=builder gateway/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
