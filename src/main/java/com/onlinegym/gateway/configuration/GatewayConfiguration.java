package com.onlinegym.gateway.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.factory.TokenRelayGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;

@Configuration
class GatewayConfiguration {

    @Value("${training-service.port}")
    private String trainingServicePort;
    @Value("${training-service.host}")
    private String trainingServiceHost;

    @Value("${statistic-service.host}")
    private String statisticServiceHost;
    @Value("${statistic-service.port}")
    private String statisticServicePort;

    @Value("${ui-service.host}")
    private String uiServiceHost;
    @Value("${ui-service.port}")
    private String uiServicePort;
    @Value("${trainers.host}")
    private String trainersHost;
    @Value("${trainers.port}")
    private String trainersPort;

    @Value("${users.host}")
    private String usersHost;
    @Value("${users.port}")
    private String usersPort;

    @Value("${server.port}")
    private String gatewayServicePort;

    @Value("${admin-ui.host}")
    private String adminUiHost;

    @Autowired
    private TokenRelayGatewayFilterFactory filterFactory;


    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        //@formatter:off
        return builder.routes()
                .route("admin", r -> r.path("/admin/**")
                        .filters(f -> f.filter(filterFactory.apply()).rewritePath("^/admin", "/"))
                        .uri(adminUiHost + ":80/"))
                .route("trainers", r -> r.path("/trainers/**")
                        .filters(f -> f.filter(filterFactory.apply()))
                        .uri(trainersHost + ":" + trainersPort))
                .route("usersService", r -> r.path("/api/users/**")
                        .filters(f -> f.filter(filterFactory.apply()).removeRequestHeader("Cookie").stripPrefix(2))
                        .uri(usersHost + ":" + usersPort))
                .route("users", r -> r.path("/users/**")
                        .filters(f -> f.filter(filterFactory.apply())
                                .rewritePath("^/users", "/auth/admin/realms/onlinegym/users"))
                        .uri("http://keycloak:8080"))
                .route("logout", r -> r.path("/auth/realms/onlinegym/protocol/openid-connect/logout**")
                        .uri("http://keycloak:8080"))
                .route("training", r -> r.path("/workout-program/**")
                        .filters(f -> f.filter(filterFactory.apply())
                                .removeRequestHeader("Cookie")
                                .rewritePath("/workout-program(?<segment>/?.*)", "$\\{segment}"))
                        .uri(trainingServiceHost + ":" + trainingServicePort + '/'))
                .route("me", r -> r.path("/user_profile/me")
                        .filters(f -> f.filter(filterFactory.apply())
                                .removeRequestHeader("Cookie"))
                        .uri(trainingServiceHost + ":" + trainingServicePort))
                .route("statistic", r -> r.path("/statistic/**")
                        .filters(f -> f.filter(filterFactory.apply()).removeRequestHeader("Cookie").stripPrefix(1))
                        .uri(statisticServiceHost + ":" + statisticServicePort))
                .route("static", r -> r.path("/ui/ru/**", "/ui/en/**")
                        .filters(f -> f.filter(filterFactory.apply()))
                        .uri(uiServiceHost + ":" + uiServicePort))
                .route("defaultRu", r -> r.header("Accept-Language", "ru.*").and().path("/**")
                        .filters(f -> f.filter(filterFactory.apply()).rewritePath("/(?<segment>/?.*)", "/ui/ru/$\\{segment}"))
                        .uri(uiServiceHost + ":" + uiServicePort))
                .route("defaultEn", r -> r.path("/**")
                        .filters(f -> f.filter(filterFactory.apply()).rewritePath("/(?<segment>/?.*)", "/ui/en/$\\{segment}"))
                        .uri(uiServiceHost + ":" + uiServicePort))
                .build();
        //@formatter:on
    }

}

