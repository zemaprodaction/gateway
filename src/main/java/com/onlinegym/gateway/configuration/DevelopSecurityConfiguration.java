package com.onlinegym.gateway.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.HttpStatusServerEntryPoint;
import org.springframework.security.web.server.csrf.CookieServerCsrfTokenRepository;

@Profile("dev")
@Configuration
public class DevelopSecurityConfiguration {

    @Autowired
    ReactiveClientRegistrationRepository clientRegistrationRepository;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(
            ServerHttpSecurity http) {
        String endSessionEndpoint = System.getenv("END-SESSION-ENDPOINT");
        return http
                .authorizeExchange()
                .pathMatchers("/admin/**").hasAuthority("SCOPE_admin_panel")
                .pathMatchers("/trainers/**").hasAnyAuthority("SCOPE_trainer_panel", "SCOPE_admin_panel")
                .anyExchange().permitAll()
                .and().cors()
                .and().formLogin().disable()
                .httpBasic().disable()
                .oauth2Login()
                .and().exceptionHandling()
                .authenticationEntryPoint(new HttpStatusServerEntryPoint(HttpStatus.UNAUTHORIZED))
                .and()
                .logout()
                .logoutSuccessHandler(
                        new CustomOidcClientInitiatedServerLogoutSuccessHandler(clientRegistrationRepository, endSessionEndpoint)
                ).and().csrf()
                .csrfTokenRepository(CookieServerCsrfTokenRepository.withHttpOnlyFalse())
                .and().build();
    }
}
