package com.onlinegym.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class GatewayApplication {
    public static void main(String[] args) {
        System.out.println(System.getenv("tr.service.port"));
        System.out.println(System.getenv("TR_SERVICE_PORT"));
        System.out.println(System.getenv());
        SpringApplication.run(GatewayApplication.class, args);
    }
}
